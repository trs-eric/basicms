﻿;  This file is part of the basiCMS project.
;  Copyright (c) 2022 Eric Canales
;  Authors: Eric Canales
; 
;  This program is free software: you can redistribute it and/or modify it
;  under the terms of the GNU Affero General Public License as published by the
;  Free Software Foundation, either version 3 of the License, or (at your
;  option) any later version.

;  This program is distributed in the hope that it will be useful, but WITHOUT
;  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
;  more details.

;  You should have received a copy of the GNU Affero General Public License
;  along with this program. If not, see <https://www.gnu.org/licenses/>.

EnableExplicit

OpenCryptRandom()

IncludeFile #PB_Compiler_File + "i" ;- PBHGEN

Structure basiCMS
  cgi_init.i
  cgi_fast_init.i
  network.i
EndStructure

Define basiCMS.basiCMS
 
XIncludeFile "libraries/uuid.pb"
XIncludeFile "libraries/http.pb"

XIncludeFile "plugins/basiCadre.pb"
XIncludeFile "plugins/database.pb"
XIncludeFile "plugins/sqlite.pb"
XIncludeFile "plugins/session.pb"

XIncludeFile "modules/home.pb"
XIncludeFile "modules/page_not_found.pb"

basiCMS\network = InitNetwork()
basiCMS\cgi_init = InitCGI()
basiCMS\cgi_fast_init = InitFastCGI(5600) ; Create the FastCGI program on port 5600

If basiCMS\network = 0 Or basiCMS\cgi_init = 0 Or basiCMS\cgi_fast_init = 0
  Debug "Failure"
  Debug "Status:"
  Debug "Network = " + Str(basiCMS\network)
  Debug "CGI Init = " + Str(basiCMS\cgi_init)
  Debug "FCGI Init = " + Str(basiCMS\cgi_fast_init)
  End
EndIf

dispatch("", "POST", "init", basiCadre::#plugins)
dispatch("", "POST", "init", basiCadre::#modules)


Repeat
  basiCadre::request\id = WaitFastCGIRequest()
  If basiCadre::request\id
    basiCadre::request\readcgi = ReadCGI()
    If basiCadre::request\readcgi = 0
      End ;an error has occurred
    EndIf
    
    Define k.i
    For k.i = 0 To CountCGIParameters()-1 
      Debug CGIParameterName(k) + ": " + CGIParameterValue("", k)
    Next
    
    basiCadre::request\uri = URLEncoder(URLDecoder(Left(CGIVariable(#PB_CGI_RequestURI), basiCadre::settings\max_uri_length)))
    ;todo, check if encoding and decoding is necessary for sanitizing
    Debug "URI: " + basiCadre::request\uri
    basiCadre::request\method = CGIVariable(#PB_CGI_RequestMethod)
      
    basiCadre::request\mod = ""
    
    ForEach basiCadre::modules() ;what module is being requested?
      If basiCadre::modules() = basiCadre::URI_vars(1,basiCadre::request\uri)
        basiCadre::request\mod = basiCadre::URI_vars(1,basiCadre::request\uri)
      EndIf
    Next
    
    ;dispatch said module
    dispatch("", "POST", "request", basiCadre::#plugins)
    dispatch("", "POST", "request", basiCadre::#modules)
    dispatch(basiCadre::request\mod, basiCadre::request\method, basiCadre::request\uri)
  EndIf
  
Until #False


Procedure dispatch(mod.s, method.s, uri.s, destination.i = 2) ; =2 should be basiCadre::#single_module, submit a bug fix for PBHGEN
  Select destination
    Case basiCadre::#plugins
      ForEach basiCadre::plugins() ;call every plugin
        basiCadre::procedure_pointer = GetRuntimeInteger(basiCadre::plugins() + "::controller_" + basiCadre::URI_vars(1, uri) + "()")
        If basiCadre::procedure_pointer <> 0
          Debug "Successfully calling: " + basiCadre::plugins() + "::controller_" + basiCadre::URI_vars(1, uri) + "()"
          CallFunctionFast(basiCadre::procedure_pointer, @method)
        Else
          Debug "Error calling: " + basiCadre::plugins() + "::controller_" + basiCadre::URI_vars(1, uri) + "()"
        EndIf      
      Next
    Case basiCadre::#modules
      ForEach basiCadre::modules() ;call every module
        basiCadre::procedure_pointer = GetRuntimeInteger(basiCadre::modules() + "::controller_" + basiCadre::URI_vars(1, uri) + "()")
        If basiCadre::procedure_pointer <> 0
          Debug "Successfully calling: " + basiCadre::modules() + "::controller_" + basiCadre::URI_vars(1, uri) + "()"
          CallFunctionFast(basiCadre::procedure_pointer, @method)
        Else
          Debug "Error calling: " + basiCadre::modules() + "::controller_" + basiCadre::URI_vars(1, uri) + "()"
        EndIf      
      Next
    Case basiCadre::#single_module
      If mod = ""
        mod = "home" ;default route
      EndIf
      ;call only needed module
      basiCadre::procedure_pointer = GetRuntimeInteger(mod+"::controller_" + basiCadre::URI_vars(1, uri) + "()")
      If basiCadre::procedure_pointer <> 0
        Debug "Successfully calling: " + mod+"::controller_" + basiCadre::URI_vars(1,uri) + "()"
        CallFunctionFast(basiCadre::procedure_pointer, @method)
      Else
        Debug "Error calling: " + mod+"::controller_" + basiCadre::URI_vars(1, uri) + "()"
        
        http::redirect("page_not_found", basiCadre::settings\install_dir, http::#redirect_301)     
;        basiCadre::procedure_pointer = GetRuntimeInteger("page_not_found::controller_page_not_found()")
;        If basiCadre::procedure_pointer <> 0
;          Debug "Successfully calling: page_not_found::controller_page_not_found()"
;          CallFunctionFast(basiCadre::procedure_pointer, @method)
;        Else
;          Debug "Error calling: page_not_found::controller_page_not_found()"
;        EndIf
      EndIf
   EndSelect
 EndProcedure

  
  
; IDE Options = PureBasic 6.00 Beta 4 (Linux - x64)
; CursorPosition = 135
; FirstLine = 105
; Folding = -
; Optimizer
; EnableXP
; EnableCompileCount = 0
; EnableBuildCount = 0
; EnableExeConstant